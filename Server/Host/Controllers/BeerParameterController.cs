﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Host.model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Host.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BeerParameterController : ControllerBase
    {
        public static readonly ParameterModel _parameters = new ParameterModel
        {
            Alcohol = 3,
            Attenuation = 8, // Percent
            MaxBitterness = 40, // IBU
            MinBitterness = 30,
            Gravity = 40,
            MaxColor = 12,
            MinColor = 6,
            Ph = 5,
            MaxTurbidity = 79.25f, // Meter
            MinTurbidity = 74.87f,
        };

        [HttpGet]
        public ParameterModel GetParameters()
        {
            return _parameters;
        }

        [HttpPost]
        public void SetParameters(ParameterModel model)
        {
            _parameters.Alcohol = model.Alcohol;
            _parameters.Attenuation = model.Attenuation; // Percent
            _parameters.MaxBitterness = model.MaxBitterness; // IBU
            _parameters.MinBitterness = model.MinBitterness;
            _parameters.Gravity = model.Gravity;
            _parameters.MaxColor = model.MaxColor;
            _parameters.MinColor = model.MinColor;
            _parameters.Ph = model.Ph;
            _parameters.MaxTurbidity = model.MaxTurbidity; // Meter
            _parameters.MinTurbidity = model.MinTurbidity;
        }
    }
}
