﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Host.model
{
    public class ParameterModel
    {
        public int Ph { get; set; }
        public int MinColor { get; set; }
        public int MaxColor { get; set; }
        public int MinBitterness { get; set; }
        public int MaxBitterness { get; set; }
        public float MinTurbidity { get; set; }
        public float MaxTurbidity { get; set; }
        public int Alcohol { get; set; }
        public int Attenuation { get; set; }
        public int Gravity { get; set; }
    }
}
