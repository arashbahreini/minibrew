(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _config_config_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./config/config.component */ "./src/app/config/config.component.ts");





var routes = [
    {
        path: '', redirectTo: 'dashboard', pathMatch: 'full'
    },
    {
        path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"]
    },
    {
        path: 'config', component: _config_config_component__WEBPACK_IMPORTED_MODULE_4__["ConfigComponent"]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n  <ul class=\"navbar-nav mr-auto mt-2 mt-lg-0\">\r\n    <li class=\"nav-item active text-white\">\r\n      <a class=\"nav-link\" routerLink=\"/dashboard\">Dashboard<span class=\"sr-only\">(current)</span></a>\r\n    </li>\r\n    <li class=\"nav-item\">\r\n      <a class=\"nav-link\" routerLink=\"/config\">Config API</a>\r\n    </li>\r\n  </ul>\r\n</nav>\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Client';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _config_config_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./config/config.component */ "./src/app/config/config.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _services_parameter_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/parameter.service */ "./src/app/services/parameter.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"],
                _config_config_component__WEBPACK_IMPORTED_MODULE_6__["ConfigComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_8__["HttpModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
            ],
            providers: [_services_parameter_service__WEBPACK_IMPORTED_MODULE_9__["ParameterService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/config/config.component.html":
/*!**********************************************!*\
  !*** ./src/app/config/config.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"border ml-2 mr-2 mt-2 mb-2 pb-2 pt-2\">\r\n  <div class=\"text-center\">\r\n    <p class=\"h4 mb-4\">Here you can config parameters API</p>\r\n  </div>\r\n  <form class=\"col-sm-11\">\r\n    <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"parameters.hasError\">\r\n      {{parameters.errorMessage}}\r\n      <button type=\"button\" class=\"btn btn-primary mr-2\" (click)=\"getParameters(true)\">Refresh\r\n        API</button>\r\n    </div>\r\n    <div class=\"form-group row\">\r\n      <label for=\"alcohol\" class=\"col-sm-2 col-form-label\">Alcohol</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"alcohol\" id=\"alcohol\"\r\n          [(ngModel)]=\"parameters.data.alcohol\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"attenuation\" class=\"col-sm-2 col-form-label\">Attenuation</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"attenuation\" id=\"attenuation\"\r\n          [(ngModel)]=\"parameters.data.attenuation\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"gravity\" class=\"col-sm-2 col-form-label\">Gravity</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"gravity\" id=\"gravity\"\r\n          [(ngModel)]=\"parameters.data.gravity\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"maxBitterness\" class=\"col-sm-2 col-form-label\">Max-Bitterness</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"maxBitterness\" id=\"maxBitterness\"\r\n          [(ngModel)]=\"parameters.data.maxBitterness\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"minBitterness\" class=\"col-sm-2 col-form-label\">Min-Bitterness</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"minBitterness\" id=\"minBitterness\"\r\n          [(ngModel)]=\"parameters.data.minBitterness\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"maxColor\" class=\"col-sm-2 col-form-label\">Max-Color</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"maxColor\" id=\"maxColor\"\r\n          [(ngModel)]=\"parameters.data.maxColor\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"minColor\" class=\"col-sm-2 col-form-label\">Min-Color</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"minColor\" id=\"minColor\"\r\n          [(ngModel)]=\"parameters.data.minColor\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"maxTurbidity\" class=\"col-sm-2 col-form-label\">Max-Turbidity</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"maxTurbidity\" id=\"maxTurbidity\"\r\n          [(ngModel)]=\"parameters.data.maxTurbidity\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"minTurbidity\" class=\"col-sm-2 col-form-label\">Min-Turbidity</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"minTurbidity\" id=\"minTurbidity\"\r\n          [(ngModel)]=\"parameters.data.minTurbidity\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"ph\" class=\"col-sm-2 col-form-label\">ph</label>\r\n      <div class=\"col-sm-5\">\r\n        <input type=\"number\" [readonly]=\"!isEditing\" class=\"form-control\" name=\"ph\" id=\"ph\"\r\n          [(ngModel)]=\"parameters.data.ph\">\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"col-md-11 text-center\">\r\n      <button type=\"button\" class=\"btn btn-danger mr-2\" (click)=\"cancelEditParameters()\"\r\n        *ngIf=\"isEditing\">Cancel</button>\r\n      <button type=\"button\" class=\"btn btn-success\" (click)=\"editParameters()\">{{isEditing ? 'Save' : 'Edit'}}</button>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/config/config.component.sass":
/*!**********************************************!*\
  !*** ./src/app/config/config.component.sass ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbmZpZy9jb25maWcuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/config/config.component.ts":
/*!********************************************!*\
  !*** ./src/app/config/config.component.ts ***!
  \********************************************/
/*! exports provided: ConfigComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigComponent", function() { return ConfigComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_parameter_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/parameter.service */ "./src/app/services/parameter.service.ts");
/* harmony import */ var _model_parameter_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/parameter.model */ "./src/app/model/parameter.model.ts");
/* harmony import */ var _model_result_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../model/result.model */ "./src/app/model/result.model.ts");





var ConfigComponent = /** @class */ (function () {
    function ConfigComponent(parameterService) {
        this.parameterService = parameterService;
        this.parameters = new _model_result_model__WEBPACK_IMPORTED_MODULE_4__["ResultModel"]();
        this.isEditing = false;
        this.copyOfParameters = new _model_parameter_model__WEBPACK_IMPORTED_MODULE_3__["ParameterModel"]();
    }
    ConfigComponent.prototype.ngOnInit = function () {
        this.getParameters();
    };
    ConfigComponent.prototype.getParameters = function () {
        var _this = this;
        this.parameters.data = new _model_parameter_model__WEBPACK_IMPORTED_MODULE_3__["ParameterModel"]();
        this.parameters.isLoading = true;
        this.parameterService.getParameters().subscribe(function (res) {
            _this.parameters.setData(res);
        }, function (error) {
            _this.parameters.setError(error);
        });
    };
    ConfigComponent.prototype.editParameters = function () {
        var _this = this;
        if (!this.isEditing) {
            this.isEditing = true;
            this.copyOfParameters = JSON.parse(JSON.stringify(this.parameters.data));
        }
        else {
            this.parameters.isLoading = true;
            this.parameterService.setParameters(this.parameters.data).subscribe(function () {
                _this.getParameters();
                _this.isEditing = false;
            }, function (error) {
                _this.getParameters();
                _this.isEditing = false;
            });
        }
    };
    ConfigComponent.prototype.cancelEditParameters = function () {
        this.parameters.data = JSON.parse(JSON.stringify(this.copyOfParameters));
        this.isEditing = false;
    };
    ConfigComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-config',
            template: __webpack_require__(/*! ./config.component.html */ "./src/app/config/config.component.html"),
            styles: [__webpack_require__(/*! ./config.component.sass */ "./src/app/config/config.component.sass")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_parameter_service__WEBPACK_IMPORTED_MODULE_2__["ParameterService"]])
    ], ConfigComponent);
    return ConfigComponent;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card border ml-2 mr-2 mt-2 mb-2 pb-2 pt-2\">\r\n  <div class=\"text-center\">\r\n    <span class=\"h4 mb-4\">Please add your brewed beer parameters</span>\r\n    <button style=\"float:right\" type=\"button\" class=\"btn btn-primary mr-2\" (click)=\"getParameters(true)\">Refresh\r\n      API</button>\r\n  </div>\r\n  <form class=\"col-sm-11\">\r\n    <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"parameters.hasError\">\r\n        {{parameters.errorMessage}}\r\n    </div>\r\n    <div class=\"form-group row\">\r\n      <label for=\"alcohol\" class=\"col-sm-2 col-form-label\">Alcohol</label>\r\n      <div class=\"col-sm-3\">\r\n        <input type=\"number\" class=\"form-control\" name=\"alcohol\" id=\"alcohol\"\r\n          (input)=\"checkAlcohol($event.target.value)\" [(ngModel)]=\"beer.alcohol.value\">\r\n      </div>\r\n      <div [ngClass]=\"'alert pt-1 pb-1' + ' ' + beer.alcohol.color\" role=\"alert\">\r\n        {{beer.alcohol.message}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"attenuation\" class=\"col-sm-2 col-form-label\">Attenuation</label>\r\n      <div class=\"col-sm-3\">\r\n        <input type=\"number\" class=\"form-control\" name=\"attenuation\" id=\"attenuation\"\r\n          (input)=\"checkAttenuation($event.target.value)\" [(ngModel)]=\"beer.attenuation.value\">\r\n      </div>\r\n      <div [ngClass]=\"'alert pt-1 pb-1' + ' ' + beer.attenuation.color\" role=\"alert\">\r\n        {{beer.attenuation.message}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"gravity\" class=\"col-sm-2 col-form-label\">Gravity</label>\r\n      <div class=\"col-sm-3\">\r\n        <input type=\"number\" class=\"form-control\" name=\"gravity\" id=\"gravity\"\r\n          (input)=\"checkGravity($event.target.value)\" [(ngModel)]=\"beer.gravity.value\">\r\n      </div>\r\n      <div [ngClass]=\"'alert pt-1 pb-1' + ' ' + beer.gravity.color\" role=\"alert\">\r\n        {{beer.gravity.message}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"bitterness\" class=\"col-sm-2 col-form-label\">Bitterness</label>\r\n      <div class=\"col-sm-3\">\r\n        <input type=\"number\" class=\"form-control\" name=\"bitterness\" id=\"bitterness\"\r\n          (input)=\"checkBitterness($event.target.value)\" [(ngModel)]=\"beer.bitterness.value\">\r\n      </div>\r\n      <div [ngClass]=\"'alert pt-1 pb-1' + ' ' + beer.bitterness.color\" role=\"alert\">\r\n        {{beer.bitterness.message}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"color\" class=\"col-sm-2 col-form-label\">Color</label>\r\n      <div class=\"col-sm-3\">\r\n        <input type=\"number\" class=\"form-control\" name=\"color\" id=\"color\" (input)=\"checkColor($event.target.value)\"\r\n          [(ngModel)]=\"beer.color.value\">\r\n      </div>\r\n      <div [ngClass]=\"'alert pt-1 pb-1' + ' ' + beer.color.color\" role=\"alert\">\r\n        {{beer.color.message}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"turbidity\" class=\"col-sm-2 col-form-label\">Turbidity</label>\r\n      <div class=\"col-sm-3\">\r\n        <input type=\"number\" class=\"form-control\" name=\"turbidity\" id=\"turbidity\"\r\n          (input)=\"checkTurbidity($event.target.value)\" [(ngModel)]=\"beer.turbidity.value\">\r\n      </div>\r\n      <div [ngClass]=\"'alert pt-1 pb-1' + ' ' + beer.turbidity.color\" role=\"alert\">\r\n        {{beer.turbidity.message}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"form-group row\">\r\n      <label for=\"ph\" class=\"col-sm-2 col-form-label\">ph</label>\r\n      <div class=\"col-sm-3\">\r\n        <input type=\"number\" class=\"form-control\" name=\"ph\" id=\"ph\" (input)=\"checkPh($event.target.value)\"\r\n          [(ngModel)]=\"beer.ph.value\">\r\n      </div>\r\n      <div [ngClass]=\"'alert pt-1 pb-1' + ' ' + beer.ph.color\" role=\"alert\">\r\n        {{beer.ph.message}}\r\n      </div>\r\n    </div>\r\n  </form>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.sass":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.sass ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LnNhc3MifQ== */"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _model_result_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/result.model */ "./src/app/model/result.model.ts");
/* harmony import */ var _model_parameter_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/parameter.model */ "./src/app/model/parameter.model.ts");
/* harmony import */ var _services_parameter_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/parameter.service */ "./src/app/services/parameter.service.ts");
/* harmony import */ var _model_beer_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../model/beer.model */ "./src/app/model/beer.model.ts");






var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(parameterService) {
        this.parameterService = parameterService;
        this.parameters = new _model_result_model__WEBPACK_IMPORTED_MODULE_2__["ResultModel"]();
        this.beer = new _model_beer_model__WEBPACK_IMPORTED_MODULE_5__["BeerModel"]();
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.getParameters(false);
    };
    DashboardComponent.prototype.checkAlcohol = function () {
        if (!this.beer.alcohol.value) {
            return;
        }
        if (this.beer.alcohol.value > this.parameters.data.alcohol) {
            this.beer.alcohol.message = 'Alcohol value should be ' + this.parameters.data.alcohol + ' but it is greather.';
            this.beer.alcohol.color = 'alert-danger';
        }
        else if (this.beer.alcohol.value < this.parameters.data.alcohol) {
            this.beer.alcohol.message = 'Alcohol value should be ' + this.parameters.data.alcohol + ' but it is less than.';
            this.beer.alcohol.color = 'alert-danger';
        }
        else {
            this.beer.alcohol.message = 'Alcohol value is in range.';
            this.beer.alcohol.color = 'alert-success';
        }
    };
    DashboardComponent.prototype.checkPh = function () {
        if (!this.beer.ph.value) {
            return;
        }
        if (this.beer.ph.value > this.parameters.data.ph) {
            this.beer.ph.message = 'PH value should be ' + this.parameters.data.ph + ' but it is greather.';
            this.beer.ph.color = 'alert-danger';
        }
        else if (this.beer.ph.value < this.parameters.data.ph) {
            this.beer.ph.message = 'PH value should be ' + this.parameters.data.ph + ' but it is less than.';
            this.beer.ph.color = 'alert-danger';
        }
        else {
            this.beer.ph.message = 'PH value is in range.';
            this.beer.ph.color = 'alert-success';
        }
    };
    DashboardComponent.prototype.checkAttenuation = function () {
        if (!this.beer.attenuation.value) {
            return;
        }
        if (this.beer.attenuation.value > this.parameters.data.attenuation) {
            this.beer.attenuation.message = 'Attenuation value should be ' + this.parameters.data.attenuation + ' but it is greather.';
            this.beer.attenuation.color = 'alert-danger';
        }
        else if (this.beer.attenuation.value < this.parameters.data.attenuation) {
            this.beer.attenuation.message = 'Attenuation value should be ' + this.parameters.data.attenuation + ' but it is less than.';
            this.beer.attenuation.color = 'alert-danger';
        }
        else {
            this.beer.attenuation.message = 'Attenuation value is in range.';
            this.beer.attenuation.color = 'alert-success';
        }
    };
    DashboardComponent.prototype.checkGravity = function () {
        if (!this.beer.gravity.value) {
            return;
        }
        if (this.beer.gravity.value > this.parameters.data.gravity) {
            this.beer.gravity.message = 'Gravity value should be ' + this.parameters.data.gravity + ' but it is greather.';
            this.beer.gravity.color = 'alert-danger';
        }
        else if (this.beer.gravity.value < this.parameters.data.gravity) {
            this.beer.gravity.message = 'Gravity value should be ' + this.parameters.data.gravity + ' but it is less than.';
            this.beer.gravity.color = 'alert-danger';
        }
        else {
            this.beer.gravity.message = 'Gravity value is in range.';
            this.beer.gravity.color = 'alert-success';
        }
    };
    DashboardComponent.prototype.checkBitterness = function () {
        if (!this.beer.bitterness.value) {
            return;
        }
        if (this.beer.bitterness.value > this.parameters.data.maxBitterness
            || this.beer.bitterness.value < this.parameters.data.minBitterness) {
            this.beer.bitterness.message = 'Bitterness value should be in range of ' +
                this.parameters.data.minBitterness + ' and ' + this.parameters.data.maxBitterness;
            this.beer.bitterness.color = 'alert-danger';
        }
        else {
            this.beer.bitterness.message = 'Bitterness value is in range.';
            this.beer.bitterness.color = 'alert-success';
        }
    };
    DashboardComponent.prototype.checkColor = function () {
        if (!this.beer.color.value) {
            return;
        }
        if (this.beer.color.value > this.parameters.data.maxColor || this.beer.color.value < this.parameters.data.minColor) {
            this.beer.color.message = 'Color value should be in range of ' +
                this.parameters.data.minColor + ' and ' + this.parameters.data.maxColor;
            this.beer.color.color = 'alert-danger';
        }
        else {
            this.beer.color.message = 'Color value is in range.';
            this.beer.color.color = 'alert-success';
        }
    };
    DashboardComponent.prototype.checkTurbidity = function () {
        if (!this.beer.turbidity.value) {
            return;
        }
        if (this.beer.turbidity.value > this.parameters.data.maxTurbidity
            || this.beer.turbidity.value < this.parameters.data.minTurbidity) {
            this.beer.turbidity.message = 'Turbidity value should be in range of ' +
                this.parameters.data.minTurbidity + ' and ' + this.parameters.data.maxTurbidity;
            this.beer.turbidity.color = 'alert-danger';
        }
        else {
            this.beer.turbidity.message = 'Turbidity value is in range.';
            this.beer.turbidity.color = 'alert-success';
        }
    };
    DashboardComponent.prototype.getParameters = function (isRefreshed) {
        var _this = this;
        this.parameters.data = new _model_parameter_model__WEBPACK_IMPORTED_MODULE_3__["ParameterModel"]();
        this.parameters.isLoading = true;
        this.parameterService.getParameters().subscribe(function (res) {
            _this.parameters.setData(res);
            if (isRefreshed) {
                _this.checkAlcohol();
                _this.checkAttenuation();
                _this.checkBitterness();
                _this.checkColor();
                _this.checkGravity();
                _this.checkPh();
                _this.checkTurbidity();
            }
        }, function (error) {
            _this.parameters.setError(error);
        });
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.sass */ "./src/app/dashboard/dashboard.component.sass")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_parameter_service__WEBPACK_IMPORTED_MODULE_4__["ParameterService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/model/beer.model.ts":
/*!*************************************!*\
  !*** ./src/app/model/beer.model.ts ***!
  \*************************************/
/*! exports provided: ReportModel, BeerModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportModel", function() { return ReportModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeerModel", function() { return BeerModel; });
var ReportModel = /** @class */ (function () {
    function ReportModel() {
        this.message = 'Please add a value';
        this.color = 'alert-secondary';
    }
    return ReportModel;
}());

var BeerModel = /** @class */ (function () {
    function BeerModel() {
        this.ph = new ReportModel();
        this.color = new ReportModel();
        this.bitterness = new ReportModel();
        this.turbidity = new ReportModel();
        this.alcohol = new ReportModel();
        this.attenuation = new ReportModel();
        this.gravity = new ReportModel();
    }
    return BeerModel;
}());



/***/ }),

/***/ "./src/app/model/parameter.model.ts":
/*!******************************************!*\
  !*** ./src/app/model/parameter.model.ts ***!
  \******************************************/
/*! exports provided: ParameterModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParameterModel", function() { return ParameterModel; });
var ParameterModel = /** @class */ (function () {
    function ParameterModel() {
    }
    return ParameterModel;
}());



/***/ }),

/***/ "./src/app/model/result.model.ts":
/*!***************************************!*\
  !*** ./src/app/model/result.model.ts ***!
  \***************************************/
/*! exports provided: ResultModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultModel", function() { return ResultModel; });
var ResultModel = /** @class */ (function () {
    function ResultModel() {
        this.hasError = false;
        this.isLoading = true;
        this.hasResult = false;
    }
    ResultModel.prototype.setData = function (data) {
        this.isLoading = false;
        this.hasError = false;
        this.hasResult = true;
        this.errorMessage = '';
        this.data = data;
    };
    ResultModel.prototype.setError = function (message) {
        this.isLoading = false;
        this.hasError = true;
        this.hasResult = false;
        this.errorMessage = message;
    };
    return ResultModel;
}());



/***/ }),

/***/ "./src/app/services/parameter.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/parameter.service.ts ***!
  \***********************************************/
/*! exports provided: ParameterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParameterService", function() { return ParameterService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators_catchError__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators/catchError */ "./node_modules/rxjs-compat/_esm5/operators/catchError.js");





var ParameterService = /** @class */ (function () {
    function ParameterService(http) {
        this.http = http;
        this.api = 'http://localhost:5000/api/BeerParameter';
    }
    ParameterService.prototype.getParameters = function () {
        return this.http.get(this.api + '/GetParameters').pipe(Object(rxjs_operators_catchError__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    ParameterService.prototype.setParameters = function (model) {
        return this.http.post(this.api + '/SetParameters', model).pipe(Object(rxjs_operators_catchError__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
    };
    ParameterService.prototype.handleError = function (error) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Faild to load API. Please make sure that your server is running.');
    };
    ParameterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ParameterService);
    return ParameterService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\work\minibrew\Client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map