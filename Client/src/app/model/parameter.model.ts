export class ParameterModel {
  public ph: number;
  public minColor: number;
  public maxColor: number;
  public minBitterness: number;
  public maxBitterness: number;
  public minTurbidity: number;
  public maxTurbidity: number;
  public alcohol: number;
  public attenuation: number;
  public gravity: number;
}
