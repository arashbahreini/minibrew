export class ReportModel {
  value: number;
  color: string;
  message: string;

  constructor() {
    this.message = 'Please add a value';
    this.color = 'alert-secondary';
  }
}

export class BeerModel {
  ph: ReportModel;
  color: ReportModel;
  bitterness: ReportModel;
  turbidity: ReportModel;
  alcohol: ReportModel;
  attenuation: ReportModel;
  gravity: ReportModel;

  constructor() {
    this.ph = new ReportModel();
    this.color = new ReportModel();
    this.bitterness = new ReportModel();
    this.turbidity = new ReportModel();
    this.alcohol = new ReportModel();
    this.attenuation = new ReportModel();
    this.gravity = new ReportModel();
  }
}


