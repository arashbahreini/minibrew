export class ResultModel<T> {
  public data: T;
  public hasError: boolean;
  public isLoading: boolean;
  public hasResult: boolean;
  public errorMessage: string;

  constructor() {
    this.hasError = false;
    this.isLoading = true;
    this.hasResult = false;
  }

  setData(data: T) {
    this.isLoading = false;
    this.hasError = false;
    this.hasResult = true;
    this.errorMessage = '';
    this.data = data;
  }

  setError(message: string) {
    this.isLoading = false;
    this.hasError = true;
    this.hasResult = false;
    this.errorMessage = message;
  }
}
