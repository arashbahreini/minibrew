import { Component, OnInit } from '@angular/core';
import { ResultModel } from '../model/result.model';
import { ParameterModel } from '../model/parameter.model';
import { ParameterService } from '../services/parameter.service';
import { BeerModel } from '../model/beer.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  public parameters: ResultModel<ParameterModel> = new ResultModel<ParameterModel>();
  constructor(private parameterService: ParameterService) { }
  public beer: BeerModel = new BeerModel();

  ngOnInit() {
    this.getParameters(false);
  }

  checkAlcohol() {
    if (!this.beer.alcohol.value) {
      return;
    }
    if (this.beer.alcohol.value > this.parameters.data.alcohol) {
      this.beer.alcohol.message = 'Alcohol value should be ' + this.parameters.data.alcohol + ' but it is higher than its expected range.';
      this.beer.alcohol.color = 'alert-danger';
    } else if (this.beer.alcohol.value < this.parameters.data.alcohol) {
      this.beer.alcohol.message = 'Alcohol value should be ' + this.parameters.data.alcohol + ' but it is lower than its expected range.';
      this.beer.alcohol.color = 'alert-danger';
    } else {
      this.beer.alcohol.message = 'Alcohol value is in range.';
      this.beer.alcohol.color = 'alert-success';
    }
  }

  checkPh() {
    if (!this.beer.ph.value) {
      return;
    }
    if (this.beer.ph.value > this.parameters.data.ph) {
      this.beer.ph.message = 'PH value should be ' + this.parameters.data.ph + ' but it is higher than its expected range.';
      this.beer.ph.color = 'alert-danger';
    } else if (this.beer.ph.value < this.parameters.data.ph) {
      this.beer.ph.message = 'PH value should be ' + this.parameters.data.ph + ' but it is lower than its expected range.';
      this.beer.ph.color = 'alert-danger';
    } else {
      this.beer.ph.message = 'PH value is in range.';
      this.beer.ph.color = 'alert-success';
    }
  }

  checkAttenuation() {
    if (!this.beer.attenuation.value) {
      return;
    }
    if (this.beer.attenuation.value > this.parameters.data.attenuation) {
      this.beer.attenuation.message = 'Attenuation value should be ' + this.parameters.data.attenuation +
        ' but it is higher than its expected range.';
      this.beer.attenuation.color = 'alert-danger';
    } else if (this.beer.attenuation.value < this.parameters.data.attenuation) {
      this.beer.attenuation.message = 'Attenuation value should be ' + this.parameters.data.attenuation +
        ' but it is lower than its expected range.';
      this.beer.attenuation.color = 'alert-danger';
    } else {
      this.beer.attenuation.message = 'Attenuation value is in range.';
      this.beer.attenuation.color = 'alert-success';
    }
  }

  checkGravity() {
    if (!this.beer.gravity.value) {
      return;
    }
    if (this.beer.gravity.value > this.parameters.data.gravity) {
      this.beer.gravity.message = 'Gravity value should be ' + this.parameters.data.gravity + ' but it is higher than its expected range.';
      this.beer.gravity.color = 'alert-danger';
    } else if (this.beer.gravity.value < this.parameters.data.gravity) {
      this.beer.gravity.message = 'Gravity value should be ' + this.parameters.data.gravity + ' but it is lower than its expected range.';
      this.beer.gravity.color = 'alert-danger';
    } else {
      this.beer.gravity.message = 'Gravity value is in range.';
      this.beer.gravity.color = 'alert-success';
    }
  }

  checkBitterness() {
    if (!this.beer.bitterness.value) {
      return;
    }
    if (this.beer.bitterness.value > this.parameters.data.maxBitterness
      || this.beer.bitterness.value < this.parameters.data.minBitterness) {
      this.beer.bitterness.message = 'Bitterness value should be in range of ' +
        this.parameters.data.minBitterness + ' and ' + this.parameters.data.maxBitterness + '.';
      this.beer.bitterness.color = 'alert-danger';
    } else {
      this.beer.bitterness.message = 'Bitterness value is in range.';
      this.beer.bitterness.color = 'alert-success';
    }
  }

  checkColor() {
    if (!this.beer.color.value) {
      return;
    }
    if (this.beer.color.value > this.parameters.data.maxColor || this.beer.color.value < this.parameters.data.minColor) {
      this.beer.color.message = 'Color value should be in range of ' +
        this.parameters.data.minColor + ' and ' + this.parameters.data.maxColor + '.';
      this.beer.color.color = 'alert-danger';
    } else {
      this.beer.color.message = 'Color value is in range.';
      this.beer.color.color = 'alert-success';
    }
  }

  checkTurbidity() {
    if (!this.beer.turbidity.value) {
      return;
    }
    if (this.beer.turbidity.value > this.parameters.data.maxTurbidity
      || this.beer.turbidity.value < this.parameters.data.minTurbidity) {
      this.beer.turbidity.message = 'Turbidity value should be in range of ' +
        this.parameters.data.minTurbidity + ' and ' + this.parameters.data.maxTurbidity + '.';
      this.beer.turbidity.color = 'alert-danger';
    } else {
      this.beer.turbidity.message = 'Turbidity value is in range.';
      this.beer.turbidity.color = 'alert-success';
    }
  }

  getParameters(isRefreshed: boolean) {
    this.parameters.data = new ParameterModel();
    this.parameters.isLoading = true;
    this.parameterService.getParameters().subscribe(
      (res: ParameterModel) => {
        this.parameters.setData(res);
        if (isRefreshed) {
          this.checkAlcohol();
          this.checkAttenuation();
          this.checkBitterness();
          this.checkColor();
          this.checkGravity();
          this.checkPh();
          this.checkTurbidity();
        }
      }, (error: any) => {
        this.parameters.setError(error);
      }
    );
  }

}
