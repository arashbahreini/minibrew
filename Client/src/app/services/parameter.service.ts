import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ParameterModel } from '../model/parameter.model';
import { catchError } from 'rxjs/operators/catchError';

@Injectable({
  providedIn: 'root'
})
export class ParameterService {

  public api = 'http://localhost:5000/api/BeerParameter';

  constructor(private http: HttpClient) { }

  public getParameters(): Observable<ParameterModel> {
    return this.http.get<ParameterModel>(this.api + '/GetParameters').pipe(catchError(this.handleError));
  }

  public setParameters(model: ParameterModel) {
    return this.http.post(this.api + '/SetParameters', model).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(
      'Faild to load API. Please make sure that your server is running.');
  }
}
