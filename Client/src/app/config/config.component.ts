import { Component, OnInit } from '@angular/core';
import { ParameterService } from '../services/parameter.service';
import { ParameterModel } from '../model/parameter.model';
import { ResultModel } from '../model/result.model';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.sass']
})
export class ConfigComponent implements OnInit {

  public parameters: ResultModel<ParameterModel> = new ResultModel<ParameterModel>();
  public isEditing = false;
  public copyOfParameters: ParameterModel = new ParameterModel();

  constructor(private parameterService: ParameterService) { }

  ngOnInit() {
    this.getParameters();
  }

  getParameters() {
    this.parameters.data = new ParameterModel();
    this.parameters.isLoading = true;
    this.parameterService.getParameters().subscribe(
      (res: ParameterModel) => {
        this.parameters.setData(res);
      }, (error: any) => {
        this.parameters.setError(error);
      }
    );
  }

  editParameters() {
    if (!this.isEditing) {
      this.isEditing = true;
      this.copyOfParameters = JSON.parse(JSON.stringify(this.parameters.data));
    } else {
      this.parameters.isLoading = true;
      this.parameterService.setParameters(this.parameters.data).subscribe(
        () => {
          this.getParameters();
          this.isEditing = false;
        }
        , (error: any) => {
          this.getParameters();
          this.isEditing = false;
        }
      );
    }
  }

  cancelEditParameters() {
    this.parameters.data = JSON.parse(JSON.stringify(this.copyOfParameters));
    this.isEditing = false;
  }
}
