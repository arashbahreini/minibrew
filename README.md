# Monitoring app for a brewing system
# Getting Started
This instruction will give you a copy of the project and help you run it on your local machine and/or web server for development and testing purpose.

## Requirements

Before you start installing the project, you should install below items in your machine:

* [NodeJs (latest version is recommended)](https://nodejs.org/en/)
* @angular/cli > 6.10 - run ```npm install -g @angular/cli```
* [.Net core SDK 2.2.x](https://dotnet.microsoft.com/download)

### Download the project
Clone the repository with the following command:
```
git clone https://arashbahreini@bitbucket.org/arashbahreini/minibrew.git
```

## Install node packages 
Open your cmd/bash and navigate to the ```Client``` folder and run ```npm install``` command.

## Run the application 
Run ```ng serve``` command in your cmd/bash (which is already navigated in ```Client``` directory)  
Now open ```http://localhost:4200``` in your browser.

## Run the API
Open cmd/bash and navigate to ```Server/Host``` directory then run ```dotnet run host.sln``` command in your cmd/bash (which is already navigated in ```Server``` directory)  

## Build application
Run ```npm run build``` command in your cmd/bash (which is already navigated in ```Client``` directory)  
The application will be built in the ```Client/dist``` folder.

## Deploy application
Run ```ng build --prod``` command in your cmd/bash (which is already navigated in ```Client``` directory)  
The application will be built, dertify, minify and deploy in the ```Client/dist``` folder.
Now you can publish the generated files in your web server (IIS,NodeJs, ...) and simply host ```index.html``` file.

## Check TypeScript code for readability, maintainability, and functionality errors
Run ```npm run lint``` command in your cmd/bash (which is already navigated in ```Client``` directory)  
```tslint``` package will check typescript codes and catch errors as well as warnings.

## PipeLine
[![Pipelines](https://img.shields.io/badge/Bitbucket-Pipelines-blue.svg?style=flat)](https://bitbucket.org/arashbahreini/minibrew/addon/pipelines/home#) 

## Authors

* **Arash Bahreini** - *Initial work* - [arashbahreini](https://bitbucket.org/arashbahreini/)

See also my [GitHub](https://github.com/arashbahreini) profile.
